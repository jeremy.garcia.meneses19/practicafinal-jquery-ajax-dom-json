
/*Modificado por: Jeremy García Meneses*/

$(document).ready(function(){

	$("#btn-buscar").on("click", function(){
		clear(); //Eliminamos elentos de busqueda pasada
		clearLeyenda() //Eliminamos leyenda de busqueda pasada		
		const palabra = $('#busqueda').val();
		clearInput(); //Limpiamos la pelicula buscada
		//console.log('Palabra a buscar: '+ palabra);
		$('#content-lista').before('<h2 id = "leyenda"> Resultados para: '+ palabra + '</h2>') //Agregamos esta leyenda antes de content-lista

		/*PONGA AQUI SU CÓDIGO*/
		/*Hacemos la consulata*/ 
		$.ajax({
			url: "https://api.themoviedb.org/3/search/movie?certification_county=MX&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query="+palabra,
			success: function(respuesta) {
				console.log(respuesta);
					//Para cada elemento en la lista de resultados (para cada pelicula)
					$.each(respuesta.results, function(index, elemento) {
						//La función crearMovieCard regresa el HTML con la estructura de la pelicula
						cardHTML = crearMovieCard(elemento); 
						$("#peliculas").append(cardHTML);
						//primero debe estar creada la card de la pelicula para poder insertar el genero en el div correspondiente.
						getGenres(elemento.genre_ids,elemento.id);
					});	

					if(respuesta.results.length === 0){
						$("#peliculas").append('<p>Coincidencias no encontradas</p>');
					}
			},
			error: function() {
				console.log("No se ha podido obtener la información");
			},
		});			

	});

});


function crearMovieCard(movie){
	//Llega el objeto JSON de UNA película, como la regresa la API
	console.log(movie.poster_path);
    //sabemos que el directorio donde se guardan las imágenes es: https://image.tmdb.org/t/p/w500/
    //el atributo movie.poster_path del objeto movie, sólo contiene el nombre de la imagen (NO la ruta completa)

    //NOTAR que se accede al objeto JSON movie con la notación de punto para acceder a los atributos (movie.original_title).
	var cardHTML =
		'<!-- CARD -->'
		+'<div class="col-md-4">'
		    +'<div class="card">'
		       +'<div class="card-header">'
		          +'<img class="card-img" src="https://image.tmdb.org/t/p/w500/'+movie.poster_path+'" alt="Card image">'
		       +'</div>'
		       +'<div class="card-body">'
		          +'<h2 class="card-title">'+movie.original_title+'</h2>'
		          +'<div class="container">'
		             +'<div class="row">'
		                +'<div class="col-4 metadata">'
		                   +'<i class="fa fa-star" aria-hidden="true"></i>'
		                   +'<p>'+movie.vote_average+'/10</p>'
		                +'</div>'
		                +'<div class="col-8 metadata" id = "genero'+movie.id+'"> </div>'
		             +'</div>'
		             +'<div class="row">'
					 	+'<p>fecha lanzamiento: '+movie.release_date+'</p>'
		             +'</div>'					 
		          +'</div>'
		          +'<p class="card-text">'+movie.overview+'.</p>'
		       +'</div>'
		    +'</div>'
		+'</div>'
		+'<!-- CARD -->';

		return cardHTML;
}

function clear(){
	$("#peliculas").empty();
}

function clearInput(){
	$('#busqueda').val('');
}

function clearLeyenda(){
	$('#leyenda').remove();
}

function getGenres(genresList,id){
	var id = '#genero'+id;
	let generos = '';
	$.ajax({
		//Hacemos una consulta a la lista de generos de las peliculas
		url: "http://api.themoviedb.org/3/genre/movie/list?api_key=3356865d41894a2fa9bfa84b2b5f59bb",
		success: function(respuesta) {
			console.log(respuesta);
				//Para cada elemento en la lista de generos genres (para cada genero)
				$.each(respuesta.genres, function(index,valor) {
					genresList.forEach(id => {
						//Si un id de generos de la pelicula coincide con los de la API lo agregamos a la cadena
						if(valor.id === id){
							console.log(valor);
							generos += '  / '+valor.name;
						}
						
					});
					
				});
				console.log(generos);
				console.log(id);
				$(id).append('<p>'+generos+'</p>');
		},
		error: function() {
			console.log("No se ha podido obtener la información");
		},
	});			
}