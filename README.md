# Práctica-Final: JQuery, Ajax, DOM, JSON

## Descripción Proyecto
Repositorio con archivos para dar funcionalidad a la interfaz de búsqueda de una página de peliculas.
Se trata de buscar coincidencias usando el API de www.themoviedb.org, según la palabra indicada por el usuario en la caja 
de búsqueda y mostrar los resultados (con algunos de sus atributos) en una lista dentro de la misma página.

## Descripción solución
* Con ayuda de AJAX se realizaron peticiones a la API de The Movie DB (https://www.themoviedb.org/) para obtener las películas que coincidieran con el texto especificado por el usuario, utilizando el estilo utilizado en la pagina de inicio se mostraron los resultados de las búsquedas con algunos de los atributos que la API ofrece. Para la petición de la busqueda de la pelicula por su nombre se utilizó la siguiente url: `https://api.themoviedb.org/3/search/movie?certification_county=MX&language=es&api_key=###&query=+palabra`

* Para el atributo del genero de películas se devolvía una lista de ID’s los cuales por medio de otra petición con AJAX a la API se obtuvieron los valores para ser agregados en el elemento html correspondiente por lo que la petición debía realizarse después de la de la película, cuando ya estuvieran creados los elementos html necesarios para insertar el género de la película. Para la petición de los generos de las peliculas se utilozó el url: `http://api.themoviedb.org/3/genre/movie/list?api_key=###`

* Para el manejo de los elementos de html se utilizó la librería JQuery y los métodos que nos proporciona para trabajar de forma dinámica.

## Autor
* García Meneses Jeremy

## Instructor
* Mario Alberto Arredondo Guzmán.
